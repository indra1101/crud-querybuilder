<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function read()
    {
        //mengambil data dari tabel mahasiswa
        $mahasiswa = DB::table('mahasiswa')->get();
        //mengirim data ke view mahasiswa
        return view('mahasiswa', ['mahasiswa' => $mahasiswa]);
    }

    public function addData()
    {
        return view('form_add');
    }

    public function tambah(Request $request)
    {
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa' => $request->nama,
            'nim_mahasiswa' => $request->nim,
            'kelas_mahasiswa' => $request->kelas,
            'prodi_mahasiswa' => $request->prodi,
            'fakultas_mahasiswa' => $request->fakultas,

        ]);
        return redirect('/home');
    }

    public function ubah($id)
    {
        // mengambil data mahasiswa berdasarkan id yang dipilih
        $mahasiswa = DB::table('mahasiswa')->where('id', $id)->get();
        // mengirim data mahasiswa yang didapat ke view form_edit.blade.php
        return view('form_edit', ['mahasiswa' => $mahasiswa]);
    }

    public function update(Request $request)
    {
        // update data mahasiswa berdasarkan id
        DB::table('mahasiswa')->where('id', $request->id)->update([
            'nama_mahasiswa' => $request->nama,
            'nim_mahasiswa' => $request->nim,
            'kelas_mahasiswa' => $request->kelas,
            'prodi_mahasiswa' => $request->prodi,
            'fakultas_mahasiswa' => $request->fakultas,
        ]);
        // kembali halaman ke halaman home
        return redirect('/home');
    }

    public function hapus($id)
    {
        // menghapus data mahasiswa berdasarkan id 
        DB::table('mahasiswa')->where('id', $id)->delete();

        // kembali halaman ke halaman home
        return redirect('/home');
    }
}
