@extends('layout')
@section('main')

<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h2 class="display-3">Edit Mahasiswa</h2>
        <br />
        @foreach($mahasiswa as $mhs)
        <form action="/home/update" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $mhs->id }}"> <br />
            <div class="form-group">
            <label style="font-weight: bolder; font-size:900">Nama</label>
            <input type="text" required="required" name="nama" value="{{ $mhs->nama_mahasiswa }}" class="form-control"> 
            </div>
            <div class="form-group">
            <label style="font-weight: bolder; font-size:900">NIM</label>
            <input type="number" required="required" name="nim" value="{{ $mhs->nim_mahasiswa }}" class="form-control"> 
            </div>
            <div class="form-group">
            <label style="font-weight: bolder; font-size:900">Kelas</label>
            <input type="text" required="required" name="kelas" value="{{ $mhs->kelas_mahasiswa }}" class="form-control"> 
            </div>
            <div class="form-group">
            <label style="font-weight: bolder; font-size:900">Prodi</label>
            <input type="text" required="required" name="prodi" value="{{ $mhs->prodi_mahasiswa }}" class="form-control"> 
            </div>
            <div class="form-group">
            <label style="font-weight: bolder; font-size:900">Fakultas</label>
            <input type="text" required="required" name="fakultas" value="{{ $mhs->fakultas_mahasiswa }}" class="form-control"> 
            </div>
            <input type="submit" value="Simpan Data" class="btn btn-primary">
        </form>
    @endforeach
    </div>
</div>
@endsection
