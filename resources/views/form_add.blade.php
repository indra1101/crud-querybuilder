@extends('layout')
@section('main')
<div class="row">
<div class="col-sm-8 offset-sm-2">
        <h2 class="display-3">Tambah Mahasiswa</h2>
        <br />
    <form action="/home/simpan" method="post">
        {{ csrf_field() }}
        <div class="form-group">
        <label style="font-weight: bolder; font-size:900">Nama</label>
        <input type="text" name="nama" required="required" class="form-control">
        </div>
        <div class="form-group">
        <label style="font-weight: bolder; font-size:900">NIM</label>
        <input type="number" name="nim" required="required" class="form-control"> 
        </div>
        <div class="form-group">
        <label style="font-weight: bolder; font-size:900">Kelas</label>
        <input type="text" name="kelas" required="required" class="form-control">
        </div>
        <div class="form-group">
        <label style="font-weight: bolder; font-size:900">Prodi</label>
        <input type="text" name="prodi" required="required" class="form-control"> 
        </div>
        <div class="form-group">
        <label style="font-weight: bolder; font-size:900">Fakultas</label>
        <input type="text" name="fakultas" required="required" class="form-control">
        </div>
        <input type="submit" value="Simpan Data" class="btn btn-primary">
    </form>
    </div>
</div>
@endsection