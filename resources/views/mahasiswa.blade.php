@extends('layout')
@section('main')
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3">Data Mahasiswa</h1>
		<p>
            <a href="/home/tambah" class="btn btn-primary">Tambah</a>
		</p>
        <table class="table table-striped">
        <thead>
            <tr>
                <th style="text-align: center;">NAMA</th>
                <th style="text-align: center;">NIM</th>
                <th style="text-align: center;">KELAS</th>
                <th style="text-align: center;">PRODI</th>
                <th style="text-align: center;">FAKULTAS</th>
                <th style="text-align: center;" >ACTION</th>
            </tr>
        </thead>
        <tbody
            @foreach($mahasiswa as $mhs)
            <tr>
                <td style="text-align: center;">{{ $mhs->nama_mahasiswa }}</td>
                <td style="text-align: center;">{{ $mhs->nim_mahasiswa }}</td>
                <td style="text-align: center;">{{ $mhs->kelas_mahasiswa}}</td>
                <td style="text-align: center;">{{ $mhs->prodi_mahasiswa}}</td>
                <td style="text-align: center;">{{ $mhs->fakultas_mahasiswa}}</td>
                <td style="text-align: center;">
                    <a href="/home/edit/{{ $mhs->id }}" class="btn btn-primary btn-sm">Edit</a>
                    <a href="/home/hapus/{{ $mhs->id }}" class="btn btn-danger btn-sm">Hapus</a>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
    <div>
</div>
@endsection
