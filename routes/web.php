<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', [App\Http\Controllers\HomeController::class,'read']);
Route::get('/home/tambah', [App\Http\Controllers\HomeController::class,'addData']);
Route::post('/home/simpan', [App\Http\Controllers\HomeController::class,'tambah']);
Route::get('/home/edit/{id}', [App\Http\Controllers\HomeController::class,'ubah']);
Route::post('/home/update', [App\Http\Controllers\HomeController::class,'update']);
Route::get('/home/hapus/{id}', [App\Http\Controllers\HomeController::class,'hapus']);